package composite;

import java.util.*;

public final class Directory extends Node implements Container {
  private final String name;
  private final List<Component> children;
  private Directory parent;

  public Directory(String name){
    super(name);
    this.name = name;
    this.parent = null;
    children = new ArrayList<Component>();
  }

  @Override
  public long getSize() {
    int size = 0;
    for (Component c:
         children) {
      size += c.getSize();
    }
    return size;
  }



  public void add(Component c) {
    this.children.add(c);
    c.setParent(this);
  }

  public void remove(Component c) {
    this.children.remove(c);
  }


}
