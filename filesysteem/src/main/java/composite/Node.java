package composite;

/**
 * Author: Jan de Rijke
 */
public abstract class Node {
	protected final String name;
	protected Directory parent;

	public Node(String name) {
		this.name = name;
		this.parent = null;
	}

	public abstract long getSize();

	public String getPath() {
		if (this.parent != null)
			return this.parent.getPath() + "\\" + this.name;
		return this.name;
	}

	public void setParent(Directory parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return this.getPath() + " (" + this.getSize() + "kb)";
	}
}
