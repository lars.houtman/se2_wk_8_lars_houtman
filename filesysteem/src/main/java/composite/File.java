package composite;

public class File extends Node implements Component {

  protected final long size;


  public File(String name, long size){
    super(name);
    this.size=size;
  }

  @Override
  public long getSize() {
    return this.size;
  }

}
